FROM dolfinx/dolfinx:nightly
ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /home/fenics/

VOLUME ./src/

RUN echo 'alias python=python3' >> ~/.bashrc

RUN apt-get update && apt-get install -y \
    python3-matplotlib 

RUN pip3 install pyvista petsc4py ipykernel