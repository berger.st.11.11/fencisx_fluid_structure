# %%

from mpi4py import MPI
from dolfinx import mesh, fem, plot
import ufl
import numpy as np
from dolfinx import default_scalar_type
from dolfinx.fem.petsc import LinearProblem

domain = mesh.create_interval(MPI.COMM_WORLD, nx=100, points=(0.0, 5.0))
V = fem.functionspace(domain, ("Lagrange", 1))

ic_facets = mesh.locate_entities_boundary(
    domain,
    dim=(domain.topology.dim - 1),
    marker=lambda x: np.isclose(x[0], 0.0),
)
dofs = fem.locate_dofs_topological(V=V, entity_dim=0, entities=ic_facets)
ic = fem.dirichletbc(value=default_scalar_type(1.0), dofs=dofs, V=V)

u = ufl.TrialFunction(V)
v = ufl.TestFunction(V)


k1 = fem.Constant(domain, 1.0)
a = (ufl.dot(ufl.grad(u), ufl.grad(v)) - k1 * ufl.dot(u, v)) * ufl.dx
a += ufl.dot(ufl.grad(u[0]), v)

f = fem.Constant(domain, 0.0)
g = fem.Constant(domain, 0.0)
L = ufl.inner(f, v) * ufl.dx


problem = LinearProblem(a, L, bcs=[ic])
uh = problem.solve()
print("Solved!")

# %%
# Viz Stuff
import matplotlib.pyplot as plt

cells, types, x = plot.vtk_mesh(V)
plt.plot(x[:, 0], uh.x.array.real, label=f"U_1")
plt.xlabel("Time")
plt.ylabel("Displacement")
plt.legend()
# plt.savefig('plot.png')
plt.show()

# %%
