# %%
import numpy as np
from mpi4py import MPI
from dolfinx import fem, mesh, default_scalar_type, mesh, plot
import ufl
from ufl import dot, dx
import basix
import matplotlib.pyplot as plt
from dolfinx.fem.petsc import LinearProblem

# Define parameters
k_1 = 1.0
k_2 = 1.0
k_12= 0.00001

m_1 = 1.0
m_2 = 1.0


v1_0 = 0.0
u1_0 = 1.0
v2_0 = 0.0
u2_0 = 0.5

relaxation_factor = 0.0


# Create mesh and define function space
n_elements = 1000
t_end = 3
domain_1 = mesh.create_interval(MPI.COMM_WORLD, n_elements, (0, t_end))
element_1 = basix.ufl.element("Lagrange", domain_1.basix_cell(), 1)
V_1 = fem.functionspace(domain_1, basix.ufl.mixed_element([element_1, element_1]))

domain_2 = mesh.create_interval(MPI.COMM_WORLD, n_elements, (0, t_end))
element_2 = basix.ufl.element("Lagrange", domain_2.basix_cell(), 1)
V_2 = fem.functionspace(domain_2, basix.ufl.mixed_element([element_2, element_2]))

V0_1, _ = V_1.sub(0).collapse()
V0_2, _ = V_2.sub(0).collapse()

prev_displ_1, prev_displ_2 = np.zeros(n_elements+1), np.zeros(n_elements+1)
displ_1, displ_2 = np.zeros(n_elements + 1), np.zeros(n_elements + 1)
load_1, load_2 = fem.Function(V0_1), fem.Function(V0_2)


# Define trial and test functions
u_1, v_1 = ufl.TrialFunctions(V_1)
u_test_1, v_test_1 = ufl.TestFunctions(V_1)

u_2, v_2 = ufl.TrialFunctions(V_2)
u_test_2, v_test_2 = ufl.TestFunctions(V_2)

# Define variational problem
a = (dot(u_1.dx(0), u_test_1) - dot(v_1, u_test_1)) * dx             # Displacement
a += (dot(v_1.dx(0), v_test_1) + k_1/m_1 * dot(u_1, v_test_1)) * dx  # Velocity

f = fem.Constant(domain_1, 0.0) # No driving force
L = dot(f, u_test_1) * dx
L += dot(f, v_test_1) * dx

# Apply boundary conditions
ic_facets = mesh.locate_entities_boundary(
    domain_1, dim=0, marker=lambda x: np.isclose(x[0], 0.0)
)
dofs = fem.locate_dofs_topological(V=V_1.sub(0), entity_dim=0, entities=ic_facets)
bc_u_1 = fem.dirichletbc(value=default_scalar_type(u1_0), dofs=dofs, V=V_1.sub(0))

dofs = fem.locate_dofs_topological(V=V_1.sub(1), entity_dim=0, entities=ic_facets)
bc_v_1 = fem.dirichletbc(value=default_scalar_type(v1_0), dofs=dofs, V=V_1.sub(1))
bcs_1 = [bc_u_1, bc_v_1]

# Apply boundary conditions
ic_facets = mesh.locate_entities_boundary(
    domain_2, dim=0, marker=lambda x: np.isclose(x[0], 0.0)
)
dofs = fem.locate_dofs_topological(V=V_2.sub(0), entity_dim=0, entities=ic_facets)
bc_u_2 = fem.dirichletbc(value=default_scalar_type(u2_0), dofs=dofs, V=V_2.sub(0))

dofs = fem.locate_dofs_topological(V=V_2.sub(1), entity_dim=0, entities=ic_facets)
bc_v_2 = fem.dirichletbc(value=default_scalar_type(v2_0), dofs=dofs, V=V_2.sub(1))
bcs_2 = [bc_u_2, bc_v_2]


# Solve problem
problem = LinearProblem(a, L, bcs=bcs_1,
    petsc_options={
        "ksp_type": "preonly",
        "pc_type": "lu",
        "pc_factor_mat_solver_type": "superlu_dist",
    })

solution_1 = problem.solve()
prev_displ_1 = displ_1
displ_1 = (1-relaxation_factor)*solution_1.sub(0).collapse().x.array + relaxation_factor * prev_displ_1

load_2.interpolate(lambda x: k_12*displ_1)


i = 1
while 1:
    if i == 3:
        break
    print(f"Iteration {i}")
    i += 1

    ### Solve Second Problem

    # Define variational problem
    a = (dot(u_2.dx(0), u_test_2) - dot(v_2, u_test_2)) * dx             # Displacement
    a += (dot(v_2.dx(0), v_test_2) + k_2/m_2 * dot(u_2, v_test_2)) * dx  # Velocity

    f = fem.Constant(domain_2, 0.0) # No driving force
    L = dot(f, u_test_2) * dx
    L += dot(f, v_test_2) * dx

    problem = LinearProblem(a, L, bcs=bcs_2,
    petsc_options={
        "ksp_type": "preonly",
        "pc_type": "lu",
        "pc_factor_mat_solver_type": "superlu_dist",
    })
    solution_2 = problem.solve()
    prev_displ_2 = displ_2
    displ_2 = (1-relaxation_factor) * solution_2.sub(0).collapse().x.array + relaxation_factor * prev_displ_2

    load_1.interpolate(lambda x:  k_12*(displ_1 - displ_2))

    ### Solve Second Problem

    # Define variational problem
    a = (dot(u_1.dx(0), u_test_1) - dot(v_1, u_test_1)) * dx             # Displacement
    a += (dot(v_1.dx(0), v_test_1) + k_1/m_1 * dot(u_1, v_test_1)) * dx  # Velocity

    f = fem.Constant(domain_1, 0.0) # No driving force
    L = dot(f, u_test_1) * dx
    L += dot(load_1, v_test_1) * dx

    problem = LinearProblem(a, L, bcs=bcs_1,
    petsc_options={
        "ksp_type": "preonly",
        "pc_type": "lu",
        "pc_factor_mat_solver_type": "superlu_dist",
    })
    solution_1 = problem.solve()

    prev_displ_1 = displ_1
    displ_1 = (1-relaxation_factor)*solution_1.sub(0).collapse().x.array + relaxation_factor * prev_displ_1


    load_2.interpolate(lambda x: k_12*(displ_2 - displ_1))



print("Solved!")


# %%

from dolfinx import geometry
# Compute FE function at visualisation points
x = np.vstack((np.linspace(0.0, t_end, n_elements), [np.zeros(n_elements)] * 2)).T
bbox = geometry.bb_tree(domain_1, domain_1.topology.dim)
candidate_cells = geometry.compute_collisions_points(bbox, x)
collided_cells = geometry.compute_colliding_cells(domain_1, candidate_cells, x)
collided_cells = collided_cells.array[collided_cells.offsets[:-1]]

print("Solved!")



# Extract solution values at each mesh point
# u_values = solution_1.sub(0).collapse().eval(x, collided_cells)
# v_values = solution_2.sub(1).collapse().eval(x, collided_cells)
# displ_1 = displ_1.eval(x, collided_cells)
# displ_2 = displ_2.eval(x, collided_cells)
# Plotting
plt.figure()
plt.plot(x[:, 0], displ_1[:-1], label="u_1(t)")
plt.plot(x[:, 0], displ_2[:-1], label="u_2(t)")
plt.xlabel("Time")
plt.ylabel("Values")
plt.legend()
plt.show()

# %%oad
