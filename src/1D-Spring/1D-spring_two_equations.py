# %%
import numpy as np
from mpi4py import MPI
from dolfinx import fem, mesh, default_scalar_type, mesh, plot
import ufl
from ufl import dot, dx
import basix
import matplotlib.pyplot as plt
from dolfinx.fem.petsc import LinearProblem

# Define parameters
k_1 = 1.0
m_1 = 1.0
v_0 = 0.0
u_0 = 1.0

# Create mesh and define function space
n_elements = 100
t_end = 10
domain = mesh.create_interval(MPI.COMM_WORLD, n_elements, (0, t_end))
element = basix.ufl.element("Lagrange", domain.basix_cell(), 1)
V = fem.functionspace(domain, basix.ufl.mixed_element([element, element]))

# Define trial and test functions
u, v = ufl.TrialFunctions(V)
u_test, v_test = ufl.TestFunctions(V)


# Load
V0, _ = V.sub(0).collapse()
load = fem.Function(V0)
load.interpolate(lambda x: np.zeros(x[0].shape[0]))

# Define variational problem
a = (dot(u.dx(0), u_test) - dot(v, u_test)) * dx             # Displacement
a += (dot(v.dx(0), v_test) + k_1/m_1 * dot(u, v_test)) * dx  # Velocity

f = fem.Constant(domain, 0.0) # No driving force
L = dot(f, u_test) * dx
L += dot(load, v_test) * dx

# Apply boundary conditions
ic_facets = mesh.locate_entities_boundary(
    domain, dim=0, marker=lambda x: np.isclose(x[0], 0.0)
)
dofs = fem.locate_dofs_topological(V=V.sub(0), entity_dim=0, entities=ic_facets)
bc_u = fem.dirichletbc(value=default_scalar_type(u_0), dofs=dofs, V=V.sub(0))

dofs = fem.locate_dofs_topological(V=V.sub(1), entity_dim=0, entities=ic_facets)
bc_v = fem.dirichletbc(value=default_scalar_type(v_0), dofs=dofs, V=V.sub(1))
bcs = [bc_u, bc_v]

# Solve problem
problem = LinearProblem(a, L, bcs=bcs,
    petsc_options={
        "ksp_type": "preonly",
        "pc_type": "lu",
        "pc_factor_mat_solver_type": "superlu_dist",
    })
solution = problem.solve()

print("Solved!")


from dolfinx import geometry
# Compute FE function at visualisation points
x = np.vstack((np.linspace(0.0, t_end, n_elements), [np.zeros(n_elements)] * 2)).T
bbox = geometry.bb_tree(domain, domain.topology.dim)
candidate_cells = geometry.compute_collisions_points(bbox, x)
collided_cells = geometry.compute_colliding_cells(domain, candidate_cells, x)
collided_cells = collided_cells.array[collided_cells.offsets[:-1]]

# Extract solution values at each mesh point
u_values = solution.sub(0).collapse().eval(x, collided_cells)
v_values = solution.sub(1).collapse().eval(x, collided_cells)
# Plotting
plt.figure()
plt.plot(x[:, 0], u_values, label="Displacement (u)")
plt.plot(x[:, 0], v_values, label="Velocity (v)")
plt.xlabel("Position")
plt.ylabel("Values")
plt.legend()
plt.show()

# %%
