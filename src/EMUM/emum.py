# %%
import pyvista
from dolfinx import mesh, fem, plot, io, default_scalar_type
from dolfinx.fem.petsc import LinearProblem
from dolfinx.io import XDMFFile
from mpi4py import MPI
import ufl
import numpy as np
from ufl import dx, grad, inner
import matplotlib.pyplot as plt


domain = mesh.create_unit_square(MPI.COMM_WORLD, 100, 100)
V = fem.functionspace(domain, ("Lagrange", 1, (domain.geometry.dim, )))

def clamped_boundary(x):
    return np.isclose(x[0], 0) | np.isclose(x[0],1) | np.isclose(x[1],1)

def fs_boundary(x):
    return np.isclose(x[1], 0)

fdim = domain.topology.dim - 1
boundary_facets = mesh.locate_entities_boundary(domain, fdim, clamped_boundary)
boundary_facets_fs = mesh.locate_entities_boundary(domain, fdim, fs_boundary)

u_D = np.array([0, 0], dtype=default_scalar_type)
bc_wall = fem.dirichletbc(u_D, fem.locate_dofs_topological(V, fdim, boundary_facets), V)


def displacement_function(x):
    values = np.zeros((2,x.shape[1]), dtype=default_scalar_type)
    values[1,:] = - 0.3*np.sin(np.pi*x[0,:])
    return values

u_fs = fem.Function(V)
u_fs.interpolate(displacement_function)
bc_fs = fem.dirichletbc(u_fs, fem.locate_dofs_topological(V, fdim, boundary_facets_fs))

bcs = [bc_wall, bc_fs]

E = 1.0
ν = 0.3
μ = E / (2.0 * (1.0 + ν))
λ = E * ν / ((1.0 + ν) * (1.0 - 2.0 * ν))

def σ(v):
    """Return an expression for the stress σ given a displacement field"""
    return 2.0 * μ * ufl.sym(grad(v)) + λ * ufl.tr(ufl.sym(grad(v))) * ufl.Identity(len(v))

u, v = ufl.TrialFunction(V), ufl.TestFunction(V)
a = inner(σ(u), grad(v)) * dx
f = fem.Constant(domain, default_scalar_type((0, 0)))
L = inner(f, v) * dx

problem = LinearProblem(a, L, bcs=bcs, petsc_options={
        "ksp_type": "preonly",
        "pc_type": "lu",
        "pc_factor_mat_solver_type": "superlu_dist",})
uh = problem.solve()


with XDMFFile(domain.comm, "out_emum/displacements.xdmf", "w") as file:
    file.write_mesh(domain)
    file.write_function(uh)
# %%
