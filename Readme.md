# Usage Guide

## Docker:
0. Install docker-desktop from [here](https://www.docker.com/products/docker-desktop)

1. Build the docker image:
```bash
docker build -t fenicsx .
```

2. Run the docker container:
```bash
docker run -it --rm -v $(pwd):/home/fenics/shared fenicsx
```
3. ???

4. Profit!

